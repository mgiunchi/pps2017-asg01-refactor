package model.Implementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Interfaces.Prisoner;

/**
 * implementazione di un prigioniero
 */
public class PrisonerImpl extends PersonImpl implements Prisoner{

	private static final long serialVersionUID = -3204660779285410481L;
	private int idPrisoner;
	private Date start;
	private Date finish;
	private List <String> crimeList = new ArrayList<>();
	private int idCell;

	/**
	 * Instantiates a new prisoner.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param birthDate the birth date
	 * @param idPrisoner the id
	 * @param start inizio della reclusione
	 * @param finish fine della reclusione
	 */
	public PrisonerImpl(String name, String surname, Date birthDate, int idPrisoner, Date start,
						Date finish,List<String> crimeList, int idCell) {
		super(name, surname, birthDate);
		this.idPrisoner=idPrisoner;
		this.start=start;
		this.finish=finish;
		this.crimeList=crimeList;
		this.idCell=idCell;
	}

	@Override
	public void addCrime(String crimine) {
        crimeList.add(crimine);
	}

	@Override
	public List<String> getCrimini(){
		return this.crimeList;
	}

	public int getIdPrigioniero() {
		return this.idPrisoner;
	}

	public void setIdPrigioniero(int idPrigioniero) {
		this.idPrisoner = idPrisoner;
	}

	public Date getInizio() {
		return this.start;
	}

	public void setInizio(Date inizio) {
		this.start = inizio;
	}

	public Date getFine() {
		return finish;
	}

	public void setFine(Date fine) {
		this.finish = finish;
	}

	public int getCellID() {
		return idCell;
	}

	public void setCellID(int cellID) {
		this.idCell = cellID;
	}

	@Override
	public String toString() {
		return "PrisonerImpl [idPrigioniero=" + idPrisoner + ", inizio=" + start + ", fine=" + finish + ", crimini="
				+ crimeList + "]";
	}

}
