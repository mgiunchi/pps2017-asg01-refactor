package model.Implementations;

import java.util.Date;
import java.util.List;

public class PrisonerImplBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int idPrisoner;
    private Date start;
    private Date finish;
    private List<String> crimeList;
    private int idCell;

    public PrisonerImplBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PrisonerImplBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PrisonerImplBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public PrisonerImplBuilder setIdPrisoner(int idPrisoner) {
        this.idPrisoner = idPrisoner;
        return this;
    }

    public PrisonerImplBuilder setStart(Date start) {
        this.start = start;
        return this;
    }

    public PrisonerImplBuilder setFinish(Date finish) {
        this.finish = finish;
        return this;
    }

    public PrisonerImplBuilder setCrimeList(List<String> crimeList) {
        this.crimeList = crimeList;
        return this;
    }

    public PrisonerImplBuilder setIdCell(int idCell) {
        this.idCell = idCell;
        return this;
    }

    public PrisonerImpl build() {
        return new PrisonerImpl(name, surname, birthDate, idPrisoner, start, finish, crimeList, idCell);
    }
}