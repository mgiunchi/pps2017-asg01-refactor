package model.Implementations;

public class CellImplBuilder {
    private int id;
    private String position;
    private int capacity;

    public CellImplBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public CellImplBuilder setPosition(String position) {
        this.position = position;
        return this;
    }

    public CellImplBuilder setCapacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public CellImpl build() {
        return new CellImpl(id, position, capacity);
    }
}