package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import Util.FileHelper;
import Util.FilePath;
import controller.Interfaces.RemovePrisonerController;
import model.Implementations.CellImpl;
import model.Interfaces.Prisoner;
import view.Interfaces.MainView;
import view.Interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerControllerImpl implements RemovePrisonerController{

	private static RemovePrisonerView removePrisonerView;
	
	/**
	 * costruttore
	 * @param removePrisonerView la view
	 */
	public RemovePrisonerControllerImpl(RemovePrisonerView removePrisonerView){
		RemovePrisonerControllerImpl.removePrisonerView=removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());
	}
	
	/**
	 * listener che si occupa di rimuovare il prigioniero
	 */
	public class RemoveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			removePrisoner();
		}
	}
	
	public void removePrisoner(){

		boolean prisonerFound=false;
		List<Prisoner> currentPrisonerList= new ArrayList<>();

		try {
			//salvo la lista dei prigionieri correnti
			currentPrisonerList=MainControllerImpl.getCurrentPrisoners();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		//ciclo tutti i prigionieri
		for(Prisoner prisoner : currentPrisonerList){
			//se l'id corrisponde elimino il prigioniero
			if(prisoner.getIdPrigioniero()==removePrisonerView.getID()){
				currentPrisonerList.remove(prisoner);
				removePrisonerView.displayErrorMessage("Prigioniero rimosso");
				//recupero la lista di celle
				List<CellImpl> cellList = InsertPrisonerControllerImpl.getCells();
				for(CellImpl cell : cellList){
					if(prisoner.getCellID()==cell.getId()){
						//diminuisco il numero di prigionieri correnti nella cella del prigioniero rimosso
						cell.setCurrentPrisoners(cell.getCurrentPrisoners()-1);
					}
				}
				try {
					//salvo la lista aggiornata di celle
					InsertPrisonerControllerImpl.setCells(cellList);
				} catch (IOException e) {
					e.printStackTrace();
				}
				prisonerFound=true;
				break;
			}

//            System.out.println(prisoner.getIdPrigioniero()+" "+prisoner.getName()+" "+prisoner.getSurname());
		}
		
		//salvo la lista aggiornata di prigionieri
		File file = new File(FilePath.CURRENT_PRISONER_FILE_PATH);
		////
        FileHelper.writeOnFile(file,currentPrisonerList);
//		FileOutputStream fileOutputStream = null;
//		try {
//			fileOutputStream = new FileOutputStream(file);
//		} catch (FileNotFoundException exception) {
//			exception.printStackTrace();
//		}
//		ObjectOutputStream os = null;
//		try {
//			os = new ObjectOutputStream(fileOutputStream);
//			os.flush();
//		} catch (IOException exception) {
//            exception.printStackTrace();
//		}
//		for(Prisoner s : currentPrisonerList){
//			try {
//				os.writeObject(s);
//			} catch (IOException exception) {
//                exception.printStackTrace();
//			}
//		}
//		try {
//			os.close();
//			fileOutputStream.close();
//		} catch (IOException exception) {
//            exception.printStackTrace();
//		}
		///
		if(!prisonerFound){
			removePrisonerView.displayErrorMessage("Prigioniero non trovato!");
		}
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			new MainControllerImpl(new MainView(removePrisonerView.getRank()));
		}
		
	}
}