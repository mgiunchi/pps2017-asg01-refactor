package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import Util.FilePath;
import model.Interfaces.Guard;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

/**
 * controller della login view
 */
public class LoginControllerImpl {

    private static final String INFO_MESSAGE_USER_PASSWORD = "Devi inserire username e password";
    private static final String ERROR_MESSAGE_WRONG_USER_PASSWORD = "Combinazion3 username/password non corretta";
    private static final String WELCOME_MESSAGE = "Benvenuto Utente ";

	private LoginView loginView;

	/**
	 * costruttore
	 * @param loginView la view
	 */
	public LoginControllerImpl(LoginView loginView){
		this.loginView=loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("Accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}
	
	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {

			List<Guard> guardList = new ArrayList<>();
			try {
				//salvo la lista delle guardie nel file
				guardList = getGuards();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			boolean rightUserPassword=false;

			//controllo che non ci siano errori
			if(loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()){
				loginView.displayErrorMessage(INFO_MESSAGE_USER_PASSWORD);
			} else{
				for (Guard guard : guardList){
					if(loginView.getUsername().equals(String.valueOf(guard.getUsername())) &&
                            loginView.getPassword().equals(guard.getPassword())){
						//effettuo il login
						rightUserPassword=true;
						loginView.displayErrorMessage(WELCOME_MESSAGE + loginView.getUsername());
						loginView.dispose();
						new MainControllerImpl(new MainView(guard.getRank()));
					}
				} if(!rightUserPassword){
					loginView.displayErrorMessage(ERROR_MESSAGE_WRONG_USER_PASSWORD);
				}

			}
		}
	}
	
	/**
	 * metodo che restutuisce la lista di guardie salvata su file
	 * @return lista di guardie
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Guard> getGuards() throws IOException, ClassNotFoundException{
		File file = new File(FilePath.GUARD_FILE_PATH);
		//se il file è vuoto restituisco un file vuoto
		if (file.length()!=0){
			FileInputStream fileInputStream = new FileInputStream(file);
			ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
			List<Guard> guardList = new ArrayList<>();
			try{
				while(true){
					//salvo ogni guardia in una lista
					Guard guardFromFile = (Guard) inputStream.readObject();
					guardList.add(guardFromFile);
				}
			}catch(EOFException eofe){}
			
			fileInputStream.close();
			inputStream.close();
			return guardList;
		}
		return new ArrayList<Guard>();
	}
}
