package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Util.FilePath;
import controller.Interfaces.InsertPrisonerController;
import model.Implementations.CellImpl;
import model.Implementations.PrisonerImplBuilder;
import model.Interfaces.Prisoner;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.MainView;

/**
 * controller della view insertprisoner
 */
public class InsertPrisonerControllerImpl implements InsertPrisonerController{

	private static final String ERROR_MESSAGE_ID = "ID già usato";
    private static final String ERROR_MESSAGE_INCOMPLETE_FIELD = "Completa tutti i campi";
    private static final String ERROR_MESSAGE_START_PRISON_DATE = "Data inizio prigionia successiva a quella di fine";
    private static final String ERROR_MESSAGE_START_TODAY_PRISON_DATE = "La data inizio prigionia deve essere " +
            "successiva a quella odierna";
    private static final String ERROR_MESSAGE_BORN_DATE = "Data di nascita successiva a quella di oggi";
    private static final String CORRECT_OPERATION_MESSAGE = "Prigioniero inserito";
    private static final String ERROR_MESSAGE_CELL_OCCUPIED = "Prova con un'altra cella";

    private final static int ID_CELL_MIN = 0;
    private final static int ID_CELL_MAX = 0;

	static InsertPrisonerView insertPrisonerView;
	
	/**
	 * costruttore
	 * @param insertPrisonerView la view
	 */
	public InsertPrisonerControllerImpl(InsertPrisonerView insertPrisonerView) {
		InsertPrisonerControllerImpl.insertPrisonerView=insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
	}
	
	public void insertPrisoner(){

		boolean correctInsert = true;
        List<Prisoner>prisonerList = new ArrayList<>();
        List<Prisoner>currentPrisonersList = new ArrayList<>();
		 //recupero le liste di prigionieri correnti e storici
		 try {
			prisonerList = MainControllerImpl.getPrisoners();
			currentPrisonersList = MainControllerImpl.getCurrentPrisoners();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        //recupero il prigioniero inserito nella view
        Prisoner newPrisoner = new PrisonerImplBuilder().setName(insertPrisonerView.getNameTextField())
                .setSurname(insertPrisonerView.getSurnameTextField()).setBirthDate(insertPrisonerView.getBirthTextField())
                .setIdPrisoner(insertPrisonerView.getPrisonerIDTextField()).setStart(insertPrisonerView.getStartTextField())
                .setFinish(insertPrisonerView.getEndTextField()).setCrimeList(insertPrisonerView.getList())
                .setIdCell(insertPrisonerView.getCellID()).build();

        //controllo che non ci siano errori
        if(isSomethingEmpty(newPrisoner)){
            insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_INCOMPLETE_FIELD);
        } else {

            for(Prisoner eachPrisoner : prisonerList){
                if(eachPrisoner.getIdPrigioniero()==newPrisoner.getIdPrigioniero() && correctInsert){
                    insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_ID);
                    correctInsert=false;
                }
            }
            Calendar today = Calendar.getInstance();
            today.set(Calendar.HOUR_OF_DAY, 0); //
            if(correctInsert){
                if(newPrisoner.getInizio().after(newPrisoner.getFine())){
                    insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_START_PRISON_DATE);
                } else if(newPrisoner.getInizio().before(today.getTime())){
                    insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_START_TODAY_PRISON_DATE);
                } else if(newPrisoner.getBirthDate().after(today.getTime())){
                    insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_BORN_DATE);
                } else{
                    //aggiungo nuovo prigioniero in entrambe le liste
                    prisonerList.add(newPrisoner);
                    currentPrisonersList.add(newPrisoner);
                    //recupero le celle salvate
                    List<CellImpl> list = getCells();
                    System.out.println(list);
                    //controllo che la cella inserita non sia piena
                    for(CellImpl cell : list){
                        if(newPrisoner.getCellID()==cell.getId()||newPrisoner.getCellID()<ID_CELL_MIN||
                                newPrisoner.getCellID()>ID_CELL_MAX){
                            if(cell.getCapacity()==cell.getCurrentPrisoners()){
                                insertPrisonerView.displayErrorMessage(ERROR_MESSAGE_CELL_OCCUPIED);
                                return;
                            }
                            //aggiungo un membro alla cella
                            cell.setCurrentPrisoners(cell.getCurrentPrisoners()+1);
                        }
                    }
                    try {
                        //salvo la lista di celle aggiornata
                        setCells(list);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    //salvo le liste di prigionieri aggiornate
                    setPrisoners(prisonerList, currentPrisonersList);
                    insertPrisonerView.displayErrorMessage(CORRECT_OPERATION_MESSAGE);
                    insertPrisonerView.setList(new ArrayList<String>());
                }
            }
        }
	}
	
	/**
	 * listener che si occupa di inserire prigionieri
	 */
	public class InsertPrisonerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisoner();
		}
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerView.dispose();
			new MainControllerImpl(new MainView(insertPrisonerView.getRank()));
		}
	}
	
	/**
	 * listener che aggiunge un crimine alla lista dei crimini del prigioniero
	 */
	public class AddCrimeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//recupero i crimini inseriti nella view
			List<String>list=insertPrisonerView.getList();
			//controllo che il crimine inserito non fosse gia presente
			if(list.contains(insertPrisonerView.getCombo())){
				insertPrisonerView.displayErrorMessage("Crimine già inserito");
			}
			else{
				//inserisco il crimine
				list.add(insertPrisonerView.getCombo());
				insertPrisonerView.setList(list);
			}
		}
		
	}
	
	public boolean isSomethingEmpty(Prisoner p){
	    return (p.getName().isEmpty()||p.getSurname().isEmpty()||p.getCrimini().size()==1);
	}

    /**
     * ritorna la situazione delle celle in una lista
     * @return lista con le celle
     */
    public static List<CellImpl> getCells(){
        File file = new File(FilePath.CELL_FILE_PATH);
        List<CellImpl>list=new ArrayList<>();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            while(true){
                //salvo la situazione delle celle in una lista
                CellImpl cellFromFile = (CellImpl) inputStream.readObject();
                list.add(cellFromFile);
            }

        }catch(EOFException eofe){} catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ritorno la lista
        return list;
    }
	
	/**
	 * salva la lista di celle aggiornata
	 * @param cellList lista di celle
	 * @throws IOException
	 */
	public static void setCells(List<CellImpl> cellList) throws IOException{
		File file = new File(FilePath.CELL_FILE_PATH);
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
		//cancella il vecchio contenuto del file
		outputStream.flush();
		fileOutputStream.flush();
		//scrive ogni cella sul file
		for(CellImpl cell : cellList){
			outputStream.writeObject(cell);
		}
		outputStream.close();
	}
	
	/**
	 * salva le liste dei prigionieri
	 * @param prisoners prigionieri di sempre
	 * @param currentPrisoners prigionieri correnti
	 */
	public static void setPrisoners(List<Prisoner>prisoners,List<Prisoner>currentPrisoners){

        setListPrisonerOnFile(prisoners,FilePath.PRISONER_FILE_PATH);
        setListPrisonerOnFile(currentPrisoners,FilePath.CURRENT_PRISONER_FILE_PATH);
        /*
		File prisonerFile = new File(PRISONER_FILE_PATH);
		File currentPrisonerFile = new File(CURRENT_PRISONER_FILE_PATH);
		FileOutputStream fileOutputStreamPrisoner = null;
		FileOutputStream fileOutputStreamCurrentPrisoner = null;
		try {
			fileOutputStreamPrisoner = new FileOutputStream(prisonerFile);
            fileOutputStreamCurrentPrisoner = new FileOutputStream(currentPrisonerFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream os = null;
		ObjectOutputStream os2 = null;
		try {
			os = new ObjectOutputStream(fileOutputStreamPrisoner);
			os2 = new ObjectOutputStream(fileOutputStreamCurrentPrisoner);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//salva il contenuto delle liste sui file
		for(Prisoner prisoner : prisoners){
			try {
				os.writeObject(prisoner);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(Prisoner currentPrisoner : currentPrisoners){
			try {
				os2.writeObject(currentPrisoner);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			os.close();
			os2.close();
			fileOutputStreamPrisoner.close();
            fileOutputStreamCurrentPrisoner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	private static void setListPrisonerOnFile(List<Prisoner>prisonerList, String filePath){
        File file = new File(filePath);
        FileOutputStream fileOutputStream = null;
        ObjectOutputStream outputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            outputStream = new ObjectOutputStream(fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
             e.printStackTrace();
        }
        //salva il contenuto della lista sui file
        for(Prisoner prisoner : prisonerList){
            try {
                outputStream.writeObject(prisoner);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}