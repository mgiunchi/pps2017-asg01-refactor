package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import Util.FileHelper;
import Util.FilePath;
import model.Implementations.MovementImpl;
import view.Interfaces.AddMovementView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller che gestisce la addMovementView
 */
public class AddMovementControllerImpl {

	private final static String ERROR_MESSAGE = "Input non valido";
	private static AddMovementView addMovementView;
	
	/**
	 * costruttore 
	 * @param addMovementView la view
	 */
	public AddMovementControllerImpl(AddMovementView addMovementView){
		AddMovementControllerImpl.addMovementView=addMovementView;
		AddMovementControllerImpl.addMovementView.addBackListener(new BackListener());
		AddMovementControllerImpl.addMovementView.addInsertListener(new InsertListener());
	}
	
	/** torna alla view precedente */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			addMovementView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(addMovementView.getRank()));
		}
	}
	
	/**
	 * listener che inserisce un movimento
	 */
	public static class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//prendo il movimento dalla view
			MovementImpl movement = new MovementImpl(addMovementView.getDesc(),addMovementView.getValue(),addMovementView.getSymbol().charAt(0));
			//se l'amount è inferiore a zero stampo l'errore
			if(movement.getAmount()<=0){
				addMovementView.displayErrorMessage(ERROR_MESSAGE);
				return;
			}
			// recupero tutti i movimenti del passato
			List<MovementImpl> movements = getMovements();
			//aggiungo il nuovo movimento
			movements.add(movement);
			// salvo la lista aggiornata
			try {
				setMovements(movements);
			} catch (IOException e) {
				e.printStackTrace();
			}
			addMovementView.displayErrorMessage(FilePath.INSERTED_MOVEMENT_MESSAGE);
		}
		
		/**
		 * ritorna la lista dei movimenti di bilancio
		 * @return lista dei movimenti
		 */
		public static List<MovementImpl> getMovements(){
            List<MovementImpl>listMovement=new ArrayList<>();
			//recupero il file dove sono salvati i movimenti
			File movement_file = new File(FilePath.MOVEMENT_FILE_PATH);
			//se è vuoto restituisco una lista vuota
			if(movement_file.length()==0){
				return listMovement;
			} else {
//                //altrimenti leggo il contenuto
//                FileInputStream fileInputStream = null;
//                try {
//                    fileInputStream = new FileInputStream(movement_file);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//                ObjectInputStream inputStream = null;
//                try {
//                    inputStream = new ObjectInputStream(fileInputStream);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    while (true) {
//                        MovementImpl s = (MovementImpl) inputStream.readObject();
//                        list.add(s);
//                    }
//                } catch (EOFException eofe) {
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    inputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                //ritorno la lista
                return FileHelper.readFromFile(FilePath.MOVEMENT_FILE_PATH,listMovement);
            }
		}
		
		/**
		 * salva la lista aggiornata di movimenti
		 * @param movementList lista dei movimenti
		 * @throws IOException
		 */
		public void setMovements(List<MovementImpl> movementList) throws IOException{
			//recupero il file contenente i movimenti
			File file = new File(FilePath.MOVEMENT_FILE_PATH);
//			FileOutputStream fileOutputStream = new FileOutputStream(file);
//			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//			//elimino il vecchio contenuto
//			objectOutputStream.flush();
//			fileOutputStream.flush();
//			//salvo nel file la lista aggiornata
//			for(MovementImpl s : list){
//				objectOutputStream.writeObject(s);
//			}
//			objectOutputStream.close();
            FileHelper.writeOnFile(file,movementList);
		}
	}
	
}