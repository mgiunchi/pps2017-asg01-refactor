package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import Util.FileHelper;
import Util.FilePath;
import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {

    private final static String ERROR_MESSAGE = "Devi inserire un nome, un cognome e un prigioniero esistente";
    private final static String CORRECT_OPERATION_MESSAGE ="Visitatore inserito";
    private final static int TWO_CHARACHTER = 2;

	private static AddVisitorsView visitorsView;
	
	/**
	 * costruttore
	 * @param view la view
	 */
	public AddVisitorsControllerImpl(AddVisitorsView view)
	{
		AddVisitorsControllerImpl.visitorsView=view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
		}
	}
	
	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public static class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {

            //recupero la lista dei visitatori e la salvo in una lista
            List<Visitor> visitorList = null;
            try {
                visitorList = getVisitors();
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
            //salvo il visitatore inserito nella view
            Visitor visitor = visitorsView.getVisitor();
            //controllo che non ci siano errori
            try {
                if (visitor.getName().length() < TWO_CHARACHTER || visitor.getSurname().length() < TWO_CHARACHTER
                        || !checkPrisonerID(visitor)){
                    visitorsView.displayErrorMessage(ERROR_MESSAGE);
                }else{
                    //inserisco il visitatore nella lista
                    visitorList.add(visitor);
                    visitorsView.displayErrorMessage(CORRECT_OPERATION_MESSAGE);
                }
            } catch (ClassNotFoundException exception) {
                exception.printStackTrace();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            //salvo la lista aggiornata
            setVisitors(visitorList);
        }
		
		/**
		 * ritorna la lista dei visitatori
		 * @return lista dei visitatori
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		public static List<Visitor> getVisitors() throws IOException, ClassNotFoundException {
			File file = new File(FilePath.VISITOR_FILE_PATH);
			//se il file è vuoto ritorno una lista vuota
			if(file.length()!=0){
				FileInputStream fileInputStream = new FileInputStream(file);
				ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
				List<Visitor> visitList = new ArrayList<>();
				//salvo il cotenuto del file in una lista
				try{
					while(true){
						Visitor visitor = (Visitor) objectInputStream.readObject();
						visitList.add(visitor);
					}
				}catch(EOFException eofe){}
				
				fileInputStream.close();
				objectInputStream.close();
				return visitList;
			}
            return new ArrayList<Visitor>();
		}
    }
	
	/**
	 * controlla se l'id del prigioniero inserita è corretta
	 * @param visitor visitatore
	 * @return true se l'id è corretto
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static boolean checkPrisonerID(Visitor visitor) throws ClassNotFoundException, IOException{
		List<Prisoner> prisonerList = MainControllerImpl.getCurrentPrisoners();
		//ciclo tutti i prigionieri
		for(Prisoner prisoner : prisonerList) {
			//se gli id conicidono restituisco true
			if(prisoner.getIdPrigioniero() == visitor.getPrisonerID()){
                return true;
            }
		}
		return false;
	}
	
	/**
	 * salva la lista dei visitatori aggiornata
	 * @param visitorList lista dei visitatori
	 */
	public static void setVisitors(List<Visitor> visitorList){
		File file = new File(FilePath.VISITOR_FILE_PATH);
        FileHelper.writeOnFile(file,visitorList);
//		FileOutputStream fileOutputStream = null;
//		try {
//			fileOutputStream = new FileOutputStream(file);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		ObjectOutputStream outputStream = null;
//		try {
//			outputStream = new ObjectOutputStream(fileOutputStream);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		//salvo su file la lista aggiornata di visitatori
//		for(Visitor visitor : visitors){
//			try {
//				outputStream.writeObject(visitor);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		try {
//			outputStream.close();
//			fileOutputStream.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
}