package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

import Util.FilePath;
import controller.Interfaces.InsertGuardController;
import model.Interfaces.Guard;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardControllerImpl implements InsertGuardController{

    private static final String ID_ERROR_MESSAGE = "ID già usato";
    private static final String FIELD_ERROR_MESSAGE = "Completa tutti i campi correttamente";
    private static final String CORRECT_OPERATION_MESSAGE = "Guardia inserita";

	private static InsertGuardView insertGuardView;
	
	/**
	 * costruttore
	 * @param insertGuardView la view
	 */
	public InsertGuardControllerImpl(InsertGuardView insertGuardView){
		InsertGuardControllerImpl.insertGuardView=insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(insertGuardView.getRank()));
		}
		
	}
	
	public void insertGuard(){
		List<Guard> guardList = null;
        boolean containError=false;
		//salvo le guardie in una lista
		try {
			guardList = LoginControllerImpl.getGuards();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		//recupero la guardia inserita nella view
		Guard guardView = insertGuardView.getGuard();
        //controllo che non ci siano errori
        if(isSomethingEmpty(guardView)){
            insertGuardView.displayErrorMessage(FIELD_ERROR_MESSAGE);
            containError=true;
        } else {
			for (Guard guard : guardList) {
				if (guard.getID() == guardView.getID()) {
					insertGuardView.displayErrorMessage(ID_ERROR_MESSAGE);
					containError = true;
				}
			}
		}
		if(!containError){
			//inserisco la guardia e salvo la lista aggiornata
			guardList.add(guardView);
			setGuards(guardList);
			insertGuardView.displayErrorMessage(CORRECT_OPERATION_MESSAGE);
		}
	}
	
	/**
	 * listener che si occupa di inserire una guardia
	 */
	public class InsertListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuard();
		}
	}
	
	public boolean isSomethingEmpty(Guard guard){
		if(guard.getName().equals("")||guard.getSurname().equals("")||guard.getRank()<1||guard.getRank()>3||guard.getID()<0||guard.getPassword().length()<6)
			return true;
		return false;
	}
	
	/**
	 * salva le guardie 
	 * @param guards lista di guardie
	 */
	public static void setGuards(List<Guard>guards){

		File file = new File(FilePath.GUARD_FILE_PATH);
		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(fileOutputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//ciclo tutte le guardie nella lista e li scrivo su file
		for(Guard guard : guards){
			try {
				os.writeObject(guard);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			os.close();
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
