package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.Interfaces.Guard;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewGuardView;

/**
 * controller che gestisce la view guard view
 */
public class ViewGuardControllerImpl {

	static ViewGuardView viewGuardView;
	
	/**
	 * costruttore
	 * @param viewGuardView la view
	 */
	public ViewGuardControllerImpl(ViewGuardView viewGuardView){
		ViewGuardControllerImpl.viewGuardView=viewGuardView;
		viewGuardView.addBackListener(new BackListener());
		viewGuardView.addViewListener(new ViewGuardListener());
	}
	
	/**
	 * imposta la view in modo da mostrare la guardia
	 */
	public static class ViewGuardListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			List<Guard> guardList = new ArrayList<>();
			boolean found = false;
			try {
				guardList=LoginControllerImpl.getGuards();
			} catch (ClassNotFoundException | IOException e1) {
				e1.printStackTrace();
			}
			viewGuards(guardList,found);

		}
		
	}
	
	/**
	 * mostra le caratteristiche della guardia nella gui
	 * @param list lista delle guardie
	 * @param found true se è stata trovata la guardia richiesta
	 */
	public static void viewGuards(List<Guard>list,boolean found){
		for(Guard guard : list){
			if(guard.getID()==viewGuardView.getID()){
				viewGuardView.setName(guard.getName());
				viewGuardView.setSurname(guard.getSurname());
				viewGuardView.setBirth(guard.getBirthDate().toString());
				viewGuardView.setRank(String.valueOf(guard.getRank()));
				viewGuardView.setTelephone(guard.getTelephoneNumber());
				found=true;
			}
		}
		if(!found)
			viewGuardView.displayErrorMessage("Guardia non trovata");
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public static class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			viewGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(viewGuardView.getRank()));
		}
	}
}
