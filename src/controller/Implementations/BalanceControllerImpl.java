package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JTable;

import controller.Interfaces.BalanceController;
import model.Implementations.MovementImpl;
import view.Interfaces.BalanceView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller che gestisce la balance view
 */
public class BalanceControllerImpl implements BalanceController{

    private final static String AMOUNT = "amount";
    private final static String DESC = "desc";
    private final static String DATA = "data";
	private static BalanceView balanceView;
	
	/**
	 * costruttore
	 * @param balanceView la view
	 */
	public BalanceControllerImpl(BalanceView balanceView){
		BalanceControllerImpl.balanceView=balanceView;
		balanceView.addBackListener(new BackListener());
		showBalance();
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			balanceView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(balanceView.getRank()));
		}
	}
	
	public void showBalance(){
		int balance=0;
		//salvo tutti i movimenti passati in una lista
		List<MovementImpl> movementList = AddMovementControllerImpl.InsertListener.getMovements();
		//li ciclo e calcolo il bilancio
		for(MovementImpl movement:movementList){
				switch(movement.getChar()){
			case '-' : balance-=movement.getAmount();
				break;
			case '+' : balance+=movement.getAmount();
				break;
			}
		}
		//stampo il bilancio
		balanceView.setLabel(String.valueOf(balance));
		//creo una tabella con tutti i movimenti
		String[] tableHeading = {"+ : -",AMOUNT,DESC,DATA};
		String[][] tableContent = new String[movementList.size()][tableHeading.length];
		for(int i=0;i<movementList.size();i++){
				tableContent[i][0]=String.valueOf(movementList.get(i).getChar());
				tableContent[i][1]=String.valueOf(movementList.get(i).getAmount());
				tableContent[i][2]=movementList.get(i).getDescr();
				tableContent[i][3]=movementList.get(i).getData1();
		}
		JTable table=new JTable(tableContent,tableHeading);
		balanceView.createTable(table);
	}
}
