package Util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileHelper {

    public static <T> void writeOnFile(File file, List<T> listToWrite){
        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream;
        try {
            fileOutputStream = new FileOutputStream(file);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.flush();
            fileOutputStream.flush();
            for(T elem : listToWrite){
                objectOutputStream.writeObject(elem);
            }
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> List readFromFile(String filePath, List<T> listFromFile) {
        listFromFile = new ArrayList<T>();
        //recupero il file dove sono salvati i movimenti
        File file = new File(filePath);
        //se è vuoto restituisco una lista vuota
        if (file.length() == 0) {
            return listFromFile;
        } else {
            //altrimenti leggo il contenuto
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            ObjectInputStream inputStream = null;
            try {
                inputStream = new ObjectInputStream(fileInputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                while (true) {
                    T elem = (T) inputStream.readObject();
                    listFromFile.add(elem);
                }
            } catch (EOFException eofe) {
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return listFromFile;
        }
    }
}
