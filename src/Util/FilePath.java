package Util;

public class FilePath {

    public static final String INSERTED_MOVEMENT_MESSAGE = "Movimento inserito";
    public static final String MOVEMENT_FILE_PATH = "res/AllMovements.txt";
    public static final String VISITOR_FILE_PATH = "res/Visitors.txt";
    public static final String GUARD_FILE_PATH = "res/GuardieUserPass.txt";
    public final static String PRISONER_FILE_PATH = "res/Prisoners.txt";
    public final static String CELL_FILE_PATH = "res/Celle.txt";
    public final static String CURRENT_PRISONER_FILE_PATH = "res/CurrentPrisoners.txt";
}
