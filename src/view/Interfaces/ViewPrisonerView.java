package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.ViewPrisonerControllerImpl.BackListener;
import controller.Implementations.ViewPrisonerControllerImpl.ViewProfileListener;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.ViewPrisoner;

public class ViewPrisonerView extends PrisonManagerJFrame implements ViewPrisoner{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7065438206105719545L;

	private final static int WIDTH = 550;
	private final static int HEIGHT = 350;
	
	private final PrisonManagerJPanel south;
	private final JButton view = new JButton("Vedi profilo");
	private final JButton back = new JButton("Indietro");
	private final PrisonManagerJPanel north;
	private final JLabel prisonerID = new JLabel("ID Prigioniero");
	private final JTextField prisonerIDTextField = new JTextField(2);
	private final JLabel name;
	private final JLabel nameTextField;
	private final JLabel surname;
	private final JLabel surnameTextField;
	private final JLabel birthDate;
	private final JLabel birthDateTextField;
	private final JLabel start;
	private final JLabel startTextField;
	private final JLabel end;
	private final JLabel end1;
	private final PrisonManagerJPanel center;
	private final PrisonManagerJPanel east;
	private final JTextArea textArea;
	private int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public ViewPrisonerView(int rank){
		this.rank=rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		center = new PrisonManagerJPanel(new SpringLayout());
		name = new JLabel("Nome:		");
		nameTextField = new JLabel();
		center.add(name);
		center.add(nameTextField);
		surname = new JLabel("Cognome:	");
		surnameTextField = new JLabel();
		center.add(surname);
		center.add(surnameTextField);
		birthDate = new JLabel("Data di nascita:	");
		birthDateTextField = new JLabel();
		center.add(birthDate);
		center.add(birthDateTextField);
		start = new JLabel("Inizio reclusione:	");
		startTextField = new JLabel();
		center.add(start);
		center.add(startTextField);
		end = new JLabel("Fine reclusione:	");
		end1 = new JLabel();
		center.add(end);
		center.add(end1);
		SpringUtilities.makeCompactGrid(center,
                5, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(prisonerID);
		north.add(prisonerIDTextField);
		this.getContentPane().add(BorderLayout.NORTH,north);
		east = new PrisonManagerJPanel(new FlowLayout());
		textArea = new JTextArea();
	    textArea.setEditable(false);
		east.add(textArea);
		this.getContentPane().add(BorderLayout.EAST, east);
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(view);
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		this.setVisible(true);
	}
	
	public void addViewListener(ViewProfileListener viewListener){
		view.addActionListener(viewListener);
	}

	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public int getID(){
		if(prisonerIDTextField.getText().equals(""))
			return -1;
		return Integer.valueOf(prisonerIDTextField.getText());
	}
	
	public void setProfile(String name, String surname, String birthDate, String start, String end){
		this.nameTextField.setText(name);
		this.surnameTextField.setText(surname);
		this.birthDateTextField.setText(birthDate);
		this.startTextField.setText(start);
		this.end1.setText(end);
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}

	/*
	public int getRank(){
		return this.rank;
	}
    */

	public void setTextArea(List<String>list){
		textArea.setText("");
		for(String s : list){
			textArea.append(s + "\n");
		}
	}
}
