package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertGuardControllerImpl.BackListener;
import controller.Implementations.InsertGuardControllerImpl.InsertListener;
import model.Implementations.GuardImplBuilder;
import model.Interfaces.Guard;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.InsertGuard;

public class InsertGuardView extends PrisonManagerJFrame implements InsertGuard{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6919464397187101572L;

	private final static int WIDTH = 450;
	private final static int HEIGHT = 400;
	private final static int NUM_ROWS = 7;
	private final static int NUM_COLS = 2;
	private final static int INITIAL_X = 6;
	private final static int INITIAL_Y = 6;
	private final static int PADDING_X = 6;
	private final static int PADDING_Y = 6;
	
	private final PrisonManagerJPanel south;
    private final JButton insert = new JButton("Inserisci");
    private final PrisonManagerJPanel north;
    private final JLabel guardID = new JLabel("ID Guardia");
    private final JTextField guardIDTextField = new JTextField(6);
    private final JLabel name = new JLabel("Nome");
    private final JTextField nameTextField = new JTextField(6);
    private final JLabel surname = new JLabel("Cognome");
    private final JTextField surnameTextField = new JTextField(6);
    private final JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa)");
    private final JTextField birthDateTextField = new JTextField(6);
    private final PrisonManagerJPanel center;
    private final JLabel guardRank = new JLabel("Grado (1-2-3)");
    private final JTextField guardRankTextField = new JTextField(8);
    private final JLabel telephoneNum = new JLabel("Numero di telefono");
    private final JTextField telephoneNumTextField = new JTextField(8);
    private final JLabel password = new JLabel("Password(6 caratt. min)");
    private final JTextField passwordTextField = new JTextField(8);
    private final JButton back = new JButton("Indietro");
    private final JLabel title = new JLabel("Inserisci una guardia");
    private String pattern = "MM/dd/yyyy";
    private SimpleDateFormat format = new SimpleDateFormat(pattern);
    //private int rank;
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public InsertGuardView(int rank){
		
		this.rank=rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
		center = new PrisonManagerJPanel(new SpringLayout());
		center.add(guardID);
		center.add(guardIDTextField);
		guardIDTextField.setText("0");
		center.add(name);
		center.add(nameTextField);
		center.add(surname);
		center.add(surnameTextField);
		center.add(birthDate);
		center.add(birthDateTextField);
		birthDateTextField.setText("01/01/1980");
		center.add(telephoneNum);
		center.add(telephoneNumTextField);
		center.add(guardRank);
		center.add(guardRankTextField);
		guardRankTextField.setText("0");
		center.add(password);
		center.add(passwordTextField);
		SpringUtilities.makeCompactGrid(center,
                NUM_ROWS, NUM_COLS, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                PADDING_X, PADDING_Y);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(insert);
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		this.setVisible(true);
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public void addInsertListener(InsertListener insertListener){
		insert.addActionListener(insertListener);
	}

	/*
	public int getRank() {
		return this.rank;
	}
	*/

	public Guard getGuard(){
		Date birth = null;
		try {
			birth = format.parse(birthDateTextField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Guard guard = new GuardImplBuilder().setName(nameTextField.getText()).setSurname(surnameTextField.getText()).setBirthDate(birth)
				.setRank(Integer.valueOf(guardRankTextField.getText())).setTelephoneNumber(telephoneNumTextField.getText())
                .setIdGuard(Integer.valueOf(guardIDTextField.getText())).setPassword(passwordTextField.getText()).build();
		return guard;
	}
}
