package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controller.Implementations.LoginControllerImpl.LoginListener;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Interfaces.Inter.Login;

public class LoginView extends PrisonManagerJFrame implements Login{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9055948983228935131L;

	private final static int WIDTH = 400;
	private final static int HEIGHT = 130;
    private final static int NUM_COLS = 8;
	
	private final PrisonManagerJPanel south;
    private final JButton login = new JButton("Login");
    private final PrisonManagerJPanel center;
    private final JLabel username = new JLabel("Username");
    private final JTextField usernameTextField = new JTextField(NUM_COLS);
    private final JLabel password = new JLabel("Password");
    private final JPasswordField passwordTextField = new JPasswordField(NUM_COLS);
    private final PrisonManagerJPanel north;
    private final JLabel title = new JLabel("Prison Manager");
	
	/**
	 * costruttore
	 */
	public LoginView(){
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(login);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		center = new PrisonManagerJPanel(new FlowLayout());
		center.add(username);
		center.add(usernameTextField);
		center.add(password);
		center.add(passwordTextField);
		this.getContentPane().add(BorderLayout.CENTER,center);
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
		this.setVisible(true);
	}
	
	public String getUsername(){
		return usernameTextField.getText();
	}
	
	public String getPassword(){
		return new String(passwordTextField.getPassword());
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public void addLoginListener(LoginListener loginListener){
		login.addActionListener(loginListener);
	}
}
