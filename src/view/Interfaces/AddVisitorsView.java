package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.AddVisitorsControllerImpl.InsertListener;
import controller.Implementations.AddVisitorsControllerImpl.BackListener;
import model.Implementations.VisitorImpl;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.AddVisitors;

/**
 * view in cui si aggiungono visitatori
 */
public class AddVisitorsView extends PrisonManagerJFrame implements AddVisitors{

    private static final long serialVersionUID = -8964073612262207713L;

	private final static int WIDTH = 450;
	private final static int HEIGHT = 400;

	private final static int NUM_ROWS = 4;
	private final static int NUM_COLS = 2;
	private final static int INITIAL_X = 6;
	private final static int INITIAL_Y = 6;
	private final static int PADDING_X = 6;
	private final static int PADDING_Y = 6;

	//private int rank;
	private String pattern = "MM/dd/yyyy";
    private SimpleDateFormat format = new SimpleDateFormat(pattern);

	final PrisonManagerJPanel south;
	final JButton insert = new JButton("Inserisci");
	final PrisonManagerJPanel north;
	final JLabel name = new JLabel("Nome : ");
	final JTextField nameTextField = new JTextField(NUM_COLS);
	final JLabel surname = new JLabel("Cognome :");
	final JTextField surnameTextField = new JTextField(NUM_COLS);
	final JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa) : ");
	final JTextField birthDateTextField = new JTextField(6);
	final PrisonManagerJPanel center;
	final JButton back = new JButton("Indietro");
	final JLabel title = new JLabel("Inserisci visitatore");
	final JLabel prisonerID = new JLabel("Id prigioniero incontrato  : ");
	final JTextField prisonerTextField = new JTextField(6);
	
	/**
	 * costruttore
	 * @param rank il rank della guardia che sta visualizzando il programma
	 */
	public AddVisitorsView(int rank)
	{	
		this.rank=rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
		center = new PrisonManagerJPanel(new SpringLayout());
		center.add(name);
		center.add(nameTextField);
		center.add(surname);
		center.add(surnameTextField);
		center.add(birthDate);
		center.add(birthDateTextField);
		birthDateTextField.setText("01/01/2017");
		center.add(prisonerID);
		center.add(prisonerTextField);
		prisonerTextField.setText("0");
		SpringUtilities.makeCompactGrid(center,
                NUM_ROWS, NUM_COLS, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                PADDING_X, PADDING_Y);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(insert);
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		this.setVisible(true);
	}
	
	public VisitorImpl getVisitor(){
		Date date = null;
		try {
			date = format.parse(birthDateTextField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		VisitorImpl visitor = new VisitorImpl(nameTextField.getText(), surnameTextField.getText(),date,Integer.valueOf(prisonerTextField.getText()));
	 return visitor;
	}

	/*
	public int getRank() {
		return this.rank;
	}
	*/

	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}
	
	public void addInsertVisitorListener(InsertListener insertListener){
		insert.addActionListener(insertListener);
	}
	 
	
	

}
