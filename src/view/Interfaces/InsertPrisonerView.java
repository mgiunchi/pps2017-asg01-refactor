package view.Interfaces;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.Implementations.InsertPrisonerControllerImpl.AddCrimeListener;
import controller.Implementations.InsertPrisonerControllerImpl.BackListener;
import controller.Implementations.InsertPrisonerControllerImpl.InsertPrisonerListener;
import view.Components.PrisonManagerJFrame;
import view.Components.PrisonManagerJPanel;
import view.Components.SpringUtilities;
import view.Interfaces.Inter.InsertPrisoner;

public class InsertPrisonerView extends PrisonManagerJFrame implements InsertPrisoner{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3914632428464622887L;

	private final static int WIDTH = 750;
    private final static int HEIGHT = 400;
    private final static int NUM_ROWS = 3;
	private final static int NUM_COLS = 1;
    private final static int INITIAL_X = 6;
    private final static int INITIAL_Y = 6;
    private final static int PADDING_X = 6;
    private final static int PADDING_Y = 6;

	private final PrisonManagerJPanel south;
	private final JButton insert = new JButton("Inserisci");
	private final PrisonManagerJPanel north;
	private final JLabel prisonerID = new JLabel("ID Prigioniero");
	private final JTextField prisonerIDTextField = new JTextField(6);
	private final JLabel name = new JLabel("Nome");
	private final JTextField nameTextField = new JTextField(6);
	private final JLabel surname = new JLabel("Cognome");
	private final JTextField surnameTextField = new JTextField(6);
	private final JLabel birthDate = new JLabel("Data di nascita (mm/gg/aaaa)");
	private final JTextField birthDateTextField = new JTextField(6);
	private final PrisonManagerJPanel east;
	private final PrisonManagerJPanel center;
	private final JLabel start = new JLabel("Inizio incarcerazione (mm/gg/aaaa)");
	private final JTextField startTextField = new JTextField(8);
	private final JLabel end = new JLabel("Fine incarcerazione (mm/gg/aaaa)");
	private final JTextField endTextField = new JTextField(8);
	private final JLabel cell = new JLabel("ID Cella");
	private final JTextField cellTextField = new JTextField(8);
	private final JButton back = new JButton("Indietro");
	private final JLabel title = new JLabel("Inserisci un prigioniero");
	private final JComboBox<?> type;
	private final JTextArea textArea;
	private final JButton add=new JButton("Aggiungi un crimine");
	private String[] crimes = {"Reati contro gli animali","Reati associativi","Blasfemia e sacrilegio","Reati economici e finanziari","Falsa testimonianza","Reati militari","Reati contro il patrimonio","Reati contro la persona","Reati nell' ordinamento italiano","Reati tributari","Traffico di droga","Casi di truffe"};
	private String pattern = "MM/dd/yyyy";
	private SimpleDateFormat format = new SimpleDateFormat(pattern);
	private Date date;
    //private int rank;
	
    /**
     * costruttore
     * @param rank il rank della guardia che sta visualizzando il programma
     */
	public InsertPrisonerView(int rank){
		
		this.rank=rank;
		this.setSize(WIDTH, HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		north = new PrisonManagerJPanel(new FlowLayout());
		north.add(title);
		this.getContentPane().add(BorderLayout.NORTH,north);
		type = new JComboBox<String>(crimes);
	    type.setSelectedItem(0);
	    textArea = new JTextArea();
	    textArea.setEditable(false);
		east = new PrisonManagerJPanel(new SpringLayout());
		east.add(type);
		east.add(textArea);
	    JScrollPane logScrollPane = new JScrollPane(add);
		east.add(logScrollPane);
		SpringUtilities.makeCompactGrid(east,
                NUM_ROWS, NUM_COLS, //rows, cols
                INITIAL_X, INITIAL_X,        //initX, initY
                PADDING_X, PADDING_Y);       //xPad, yPad
		this.getContentPane().add(BorderLayout.EAST,east);
		center = new PrisonManagerJPanel(new SpringLayout());
		center.add(prisonerID);
		center.add(prisonerIDTextField);
        prisonerIDTextField.setText("0");
		center.add(name);
		center.add(nameTextField);
		center.add(surname);
		center.add(surnameTextField);
		center.add(birthDate);
		center.add(birthDateTextField);
		birthDateTextField.setText("01/01/1980");
		center.add(start);
		center.add(startTextField);
		startTextField.setText("01/01/2018");
		center.add(end);
		center.add(endTextField);
		endTextField.setText("01/01/2018");
		center.add(cell);
		center.add(cellTextField);
		cellTextField.setText("0");
		SpringUtilities.makeCompactGrid(center,
                7, 2, //rows, cols
                INITIAL_X, INITIAL_Y,        //initX, initY
                PADDING_X, PADDING_Y);       //xPad, yPad
		this.getContentPane().add(BorderLayout.CENTER,center);
		south = new PrisonManagerJPanel(new FlowLayout());
		south.add(insert);
		south.add(back);
		this.getContentPane().add(BorderLayout.SOUTH,south);
		this.setVisible(true);
	}
	
	public void addInsertPrisonerListener(InsertPrisonerListener addPrisonerListener){
		insert.addActionListener(addPrisonerListener);
	}
	
	public void addBackListener(BackListener backListener){
		back.addActionListener(backListener);
	}

	public int getPrisonerIDTextField() {
		return Integer.valueOf(prisonerIDTextField.getText());
	}

	public String getNameTextField() {
		return nameTextField.getText();
	}

	public String getSurnameTextField() {
		return surnameTextField.getText();
	}

	public Date getStartTextField() {
		try {
			date = format.parse(startTextField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public Date getEndTextField() {
		try {
			date = format.parse(endTextField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public Date getBirthTextField() {
		try {
			date = format.parse(birthDateTextField.getText());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public int getCellID() {
		return Integer.valueOf(this.cellTextField.getText());
	}
	
	public void displayErrorMessage(String error){
		JOptionPane.showMessageDialog(this, error);
	}

	/*
	public int getRank(){
		return this.rank;
	}
	*/

	public void setList(List<String>list){
		textArea.setText("");
		for(String elementOfList : list){
			textArea.append(elementOfList+"\n");
		}
	}
	
	public List<String> getList(){
		 String stringArray[] = textArea.getText().split("\\r?\\n");
		 ArrayList<String> list = new ArrayList<>(Arrays.asList(stringArray));
		 return list;
	}
	
	public String getCombo(){
		return type.getSelectedItem().toString();
	}
	
	public void addAddCrimeListener(AddCrimeListener addCrimeListener){
		add.addActionListener(addCrimeListener);
	}
	
}