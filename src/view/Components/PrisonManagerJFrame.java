package view.Components;

import javax.swing.*;
import java.awt.*;

/**
 * jframe predefinito per il programma
 */
public class PrisonManagerJFrame extends JFrame{

    protected JTable table;
    protected PrisonManagerJPanel center;
    protected int rank;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4583640093618196192L;

	/**logo programma*/
	String logoPath="res/logo.png";
	ImageIcon img = new ImageIcon(logoPath);
	
	/**costruttore jframe*/
	public PrisonManagerJFrame(){
		this.setIconImage(img.getImage());
		setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

    public int getRank(){
        return this.rank;
    }

    public void createTable(JTable table){
        this.table=table;
        table.setPreferredScrollableViewportSize(new Dimension(500,300));
        table.setFillsViewportHeight(true);
        JScrollPane js=new JScrollPane(table);
        js.setVisible(true);
        center.add(js);
    }
}
