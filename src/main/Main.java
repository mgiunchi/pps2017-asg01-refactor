package main;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Util.FileHelper;
import Util.FilePath;
import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellImpl;
import model.Implementations.CellImplBuilder;
import model.Implementations.GuardImpl;
import model.Implementations.GuardImplBuilder;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

    private static final String DEFAULT_DATE_OF_BIRTH = "01/01/1980";
    private static final String DATE_PATTERN = "MM/dd/yyyy";
    private static final int CELL_CAPACITY_FIRST_FLOOR = 4;
    private static final int CELL_CAPACITY_SECOND_FLOOR = 3;
    private static final int CELL_CAPACITY_THIRD_FLOOR = 4;
    private static final int CELL_CAPACITY_UNDERGROUND = 1;
    private static final int NUM_CELL_FIRST_FLOOR = 20;
    private static final int NUM_CELL_SECOND_FLOOR = 40;
    private static final int NUM_CELL_THIRD_FLOOR = 45;
    private static final int TOTAL_CELL = 50;

    private static final String FIRST_FLOOR = "Primo piano";
    private static final String SECOND_FLOOR = "Secondo piano";
    private static final String THIRD_FLOOR = "Terzo piano";
    private static final String UNDERGROUND_FLOOR = "Piano sotterraneo, celle di isolamento";


	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	 public static void main(final String... args){
		 //create folder in which put data
		 String Dir = "res";
		 new File(Dir).mkdir();
		 //create file for guards
		 File file = new File(FilePath.GUARD_FILE_PATH);
		 //if file is not initialized, starts initialization
		 if(file.length()==0){
			 try {
				initializeGuards(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 //read file with cells data
		 file = new File(FilePath.CELL_FILE_PATH);
		 //se il file non è ancora stato inizializzato lo faccio ora
		 if(file.length()==0){
			 try {
				initializeCells(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 
		 //instantiate controller and login
		 new LoginControllerImpl(new LoginView());
	 }
	 
	 /**
	  * initialize cells
	  * @param file file in cui creare le celle
	  * @throws IOException
	  */
	 static void initializeCells(File file) throws IOException{

		 List<CellImpl>cellList=new ArrayList<>();
		 CellImpl cellImpl;
		 for(int cellPosition=0;cellPosition<TOTAL_CELL;cellPosition++){
			 if(cellPosition<NUM_CELL_FIRST_FLOOR){
				  cellImpl = new CellImplBuilder().setId(cellPosition).setPosition(FIRST_FLOOR)
                          .setCapacity(CELL_CAPACITY_FIRST_FLOOR).build();
			 }
			 else
				 if(cellPosition<NUM_CELL_SECOND_FLOOR){
					  cellImpl = new CellImplBuilder().setId(cellPosition).setPosition(SECOND_FLOOR)
                              .setCapacity(CELL_CAPACITY_SECOND_FLOOR).build();
				 }
				 else
					 if(cellPosition<NUM_CELL_THIRD_FLOOR){
					  cellImpl = new CellImplBuilder().setId(cellPosition).setPosition(THIRD_FLOOR)
                              .setCapacity(CELL_CAPACITY_THIRD_FLOOR).build();
				 }
					 else{
						  cellImpl = new CellImplBuilder().setId(cellPosition).setPosition(UNDERGROUND_FLOOR)
                                  .setCapacity(CELL_CAPACITY_UNDERGROUND).build();
					 }
             cellList.add(cellImpl);
		 }
         FileHelper.writeOnFile(file,cellList);
//        FileOutputStream fileOutputStreamStream = new FileOutputStream(file);
//        ObjectOutputStream os = new ObjectOutputStream(fileOutputStreamStream);
//        os.flush();
//        fileOutputStreamStream.flush();
//        for(CellImpl c1 : list){
//            os.writeObject(c1);
//        }
//        os.close();
	 }
	 
	 /**
	  * metodo che inizializza le guardie
	  * @param file file in cui creare le guardie
	  * @throws IOException
	  */
	 static void initializeGuards(File file) throws IOException{

		 SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
		 Date date = null;
		try {
			date = format.parse(DEFAULT_DATE_OF_BIRTH);
		} catch (ParseException e) {
			e.printStackTrace();
		}
        List<GuardImpl> initialGuardList = new ArrayList<>();
		GuardImpl guard1= new GuardImplBuilder().setName("Oronzo").setSurname("Cantani").setBirthDate(date).setRank(1)
                .setTelephoneNumber("0764568").setIdGuard(1).setPassword("ciao01").build();
		initialGuardList.add(guard1);
		GuardImpl guard2= new GuardImplBuilder().setName("Emile").setSurname("Heskey").setBirthDate(date).setRank(2)
                .setTelephoneNumber("456789").setIdGuard(2).setPassword("asdasd").build();
		initialGuardList.add(guard2);
		GuardImpl guard3= new GuardImplBuilder().setName("Gennaro").setSurname("Alfieri").setBirthDate(date).setRank(3)
                .setTelephoneNumber("0764568").setIdGuard(3).setPassword("qwerty").build();
		initialGuardList.add(guard3);

		FileHelper.writeOnFile(file,initialGuardList);
//		FileOutputStream fileOutputStream = new FileOutputStream(file);
//		ObjectOutputStream os = new ObjectOutputStream(fileOutputStream);
//		os.flush();
//		fileOutputStream.flush();
//		for(GuardImpl initialGuard : initialGuardList){
//			os.writeObject(initialGuard);
//		}
//		os.close();

	 }
	 
}
